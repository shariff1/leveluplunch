package com.leveluplunch;

import java.io.*;

public class UpperFile {
    public static void main(String[] args) {
        readFile();
        // writeFile();

    }

    private static void readFile() {
        BufferedWriter bufferedWriter = null;
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("/home/zoheb/Desktop/README"));
            bufferedWriter = new BufferedWriter(new FileWriter
                    ("/home/zoheb/java/lunch/src/resources/withdrawals.txt"));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                bufferedWriter.write(line.toUpperCase());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (bufferedReader != null) {
                    bufferedReader.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
