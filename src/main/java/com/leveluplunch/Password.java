package com.leveluplunch;

import java.util.Scanner;

public class Password {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();
        PassVerifier passVerifier = new PassVerifier(password);
        boolean validPassword = passVerifier.isValid();
        System.out.println("The password of " + password + "is "
                + (validPassword ? " valid password " : " an invalid password"));

    }


}
