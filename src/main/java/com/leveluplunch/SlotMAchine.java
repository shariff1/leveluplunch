package com.leveluplunch;

import java.util.*;

public class SlotMAchine {
    public static void main(String[] args) {
        setUp();
    }

    private static void setUp() {
        Scanner scanner = new Scanner(System.in);
        int money = scanner.nextInt();
        prepareSlotMachine(money, scanner);
    }

    private static void prepareSlotMachine(int money,
                                           Scanner scanner) {
        List<String> images = Arrays.asList("Cherries", "Oranges", "Plums", "Bells", "Melons", "Bars");
        StringBuilder fruits = new StringBuilder(new String());
        List<Integer> integerList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            int ff = randInt(0, 5);
            fruits.append(images.get(ff)).append("--");
            integerList.add(ff);
        }
        System.out.println(fruits);
        int price = 1;
        for (int i = 0; i < integerList.size(); i++) {
            for (int j = i + 1; j < integerList.size(); j++) {

                int result = integerList.get(i).compareTo(integerList.get(j));
                if (result > 0) {
                    // 1 is greater
                    System.out.println("LOST");
                } else if (result < 0) {
                    // -1 is lesser
                    System.out.println("LOST");
                } else {
                    // 0 is equal
                    System.out.println(images.get(integerList.get(i)) + "--" + images.get(integerList
                            .get(j)));
                    if (Objects.equals(images.get(integerList.get(i)), images.get(integerList
                            .get(j)))) {
                        Locale uk = new Locale("en", "GB");
                        Currency pound = Currency.getInstance("GBP");
                        System.out.println("You won -" + (money * ++price) + pound.getSymbol(uk));
                    }
                }

            }
        }
        System.out.println("Press zero play again or One to quit");
        switch (scanner.nextInt()) {
            case 0:
                SlotMAchine.setUp();
                break;
            case 1:
                System.out.println("BAH-BYE");
                break;
            default:
                System.out.println("Enter value is not valid");
                break;
        }


    }

    private static int randInt(int min,
                               int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

}
