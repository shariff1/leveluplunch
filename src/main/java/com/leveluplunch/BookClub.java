package com.leveluplunch;

import java.util.Date;
import java.util.Scanner;

public class BookClub {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int books = scanner.nextInt();
        Date date = new Date();
        if (books == 0){
            System.out.println("Today"+ date.toGMTString()+ " 0 points.");
        }else if (books ==1){
            System.out.println("Today"+ date.toGMTString()+ " 5 points.");
        }else if (books ==2){
            System.out.println("Today"+ date.toGMTString()+ " 15 points.");
        }else if (books ==3){
            System.out.println("Today"+ date.toGMTString()+ " 30 points.");
        }else if (books ==4){
            System.out.println("Today"+ date.toGMTString()+ " 60 points.");

        }
    }
}
