package com.leveluplunch;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Tic {
    static Character player;

    public static void main(String[] args) {
        // populate the board
        // print board
        // X --- O player
        // x turn - if 1st cell selected
        // check if already selected
        // check winner
        // print the new board
        // o turn and repeat the steps

        List<String> cells = populateBoard();
        player = 'X';
        String winner = null;
        printBoard(cells);
        System.out.println("Player X will start playing");
        System.out.println("Choose the number in board");
        Scanner scanner = new Scanner(System.in);
        while (winner == null) {
            for (int i = 0; i < 8; i++) {
                int player = i % 2;
                Character choosePlayer = choosePlayer(player);
                int move = scanner.nextInt();
                cells.set(move - 1, String.valueOf(choosePlayer));
                printBoard(cells);
                System.out.println(choosePlayer + " Played");
                winner = checkWinners(cells);
                if (winner != null) {
                    System.out.println("Winner is " + winner);
                }
            }
        }
    }

    private static String checkWinners(List<String> cells
    ) {
        String line = null;
        for (int i = 0; i < 8; i++) {
            switch (i) {
                case 0:
                    line = cells.get(0) + cells.get(1) + cells.get(2);
                    break;
                case 1:
                    line = cells.get(3) + cells.get(4) + cells.get(5);
                    break;
                case 2:
                    line = cells.get(6) + cells.get(7) + cells.get(8);
                    break;
                case 3:
                    line = cells.get(0) + cells.get(3) + cells.get(6);
                    break;
                case 4:
                    line = cells.get(1) + cells.get(4) + cells.get(7);
                    break;
                case 5:
                    line = cells.get(2) + cells.get(5) + cells.get(8);
                    break;
                case 6:
                    line = cells.get(0) + cells.get(4) + cells.get(8);
                    break;
                case 7:
                    line = cells.get(2) + cells.get(4) + cells.get(6);
                    break;

            }
            if (line.equals("XXX")) {
                return "X";
            } else if (line.equals("OOO")) {
                return "O";
            }
        }

        return null;
    }

    private static Character choosePlayer(int player) {
        switch (player) {
            case 0:
                return 'X';
            case 1:
                return 'O';
        }

        return null;
    }


    private static void printBoard(List<String> cells) {
        System.out.println("/---|---|---\\");
        System.out.println("| " + cells.get(0) + " | " + cells.get(1) + " | " + cells.get(2) + " |");
        System.out.println("|-----------|");
        System.out.println("| " + cells.get(3) + " | " + cells.get(4) + " | " + cells.get(6) + " |");
        System.out.println("|-----------|");
        System.out.println("| " + cells.get(5) + " | " + cells.get(7) + " | " + cells.get(8) + " |");
        System.out.println("/---|---|---\\");
    }

    private static List<String> populateBoard() {
        List<String> cells = new LinkedList<>();
        for (int i = 1; i < 10; i++) {
            cells.add(String.valueOf(i));
        }
        return cells;
    }
}
