package com.leveluplunch;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ludo {
    public static void main(String[] args) {
        //  take two inputs
        //  generate random from two inputs
        //  frm two random input = sum them
        //  if sum = 2, 3, or 12 ===> loose
        //  if sum =  7 or 11==> win
        //  4, 5, 6, 8, 9, or 10===> point added
        generateDice();
    }

    private static void generateDice() {
        List<Integer> firstDice = new ArrayList<>();
        List<Integer> secondDice = new ArrayList<>();
        int firstAdd = 0;
        int secondAdd = 0;

        Random random = new Random();
        firstDice.add(random.nextInt(6 - 1) + 1);
        Random randomTwo = new Random();
        secondDice.add(randomTwo.nextInt(6 - 1) + 1);
        for (Integer first : firstDice
                ) {
            firstAdd += first;
        }
        for (Integer second : secondDice
                ) {
            secondAdd += second;
        }
        System.out.println(firstAdd + secondAdd);
        int sum = firstAdd + secondAdd;
        if (sum == 2 || sum == 3 || sum == 12) {
            System.out.println("YOU LOSE");
        } else if (sum == 7 || sum == 11) {
            System.out.println("YOU WIN");
        } else if (sum == 4 || sum == 5 || sum == 6 || sum == 8 || sum == 9 || sum == 10) {
            System.out.println("POINT Added");
        }
    }
}
