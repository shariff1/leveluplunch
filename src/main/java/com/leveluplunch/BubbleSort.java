package com.leveluplunch;

import java.util.*;

public class BubbleSort {
    public static void main(String[] args) {
        implementOldStyle();
/*
        Scanner scanner = new Scanner(System.in);
*/
      /*  List<Integer> integerList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            integerList.add(scanner.nextInt());
        }
        Map<Object, Long> objectLongMap = integerList.stream().collect(Collectors.groupingBy(o -> o, Collectors
                .counting()));
        objectLongMap.forEach((o, aLong) -> System.out.println(o + ":" + aLong));*/
    }

    private static void implementOldStyle() {
        List<Integer> numbers = generateRand();
        Map<Integer, Integer> table = chechRepeatingNum(numbers);
        for (Integer key : table.keySet()
                ) {
            System.out.println(key + " occur " + table.get(key) + " times");
        }

    }

    private static List<Integer> generateRand() {
        List<Integer> numbers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            numbers.add(random.nextInt((100 - 1) + 1) + 1);
        }
        return numbers;
    }

    private static Map<Integer, Integer> chechRepeatingNum(List<Integer> numbers) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer num : numbers
                ) {
            if (map.containsKey(num)) {
                int count = map.get(num);
                count = count + 1;
                map.put(num, count);
            } else {
                map.put(num, 1);
            }
        }
        return map;
    }
}
