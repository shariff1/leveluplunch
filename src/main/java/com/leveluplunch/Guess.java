package com.leveluplunch;

import java.util.Random;
import java.util.Scanner;

public class Guess {
    public static void main(String[] args) {
        Random random = new Random();
        int numbertoguess = random.nextInt(10);
        int tries = 0;
        Scanner scanner = new Scanner(System.in);
        int guess = scanner.nextInt();
        while (guess != numbertoguess) {
            tries++;
            System.out.println(guess < numbertoguess ? "guessed to less" : "Sorry, that's too high.");
            System.out.print("Guess again: ");
            guess = scanner.nextInt();
            System.out.println(guess == numbertoguess ? "YOU WIN" : "");

        }
        System.out.println(guess == numbertoguess ? "winner":"");
        System.out.println("you took tries " + tries);

    }
}
