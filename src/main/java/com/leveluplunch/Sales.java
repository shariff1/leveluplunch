package com.leveluplunch;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Sales {

    public static void main(String[] args) {
        int count = 0;
        scanFile(count);
    }

    private static void scanFile(int count) {
        String line = "";
        String[] strings = new String[0];
        Double aDouble;
        List<Double> doubles = new ArrayList<>();
        List<String> maxList = new ArrayList<>();
        List<String> minList = new ArrayList<>();


        try {
            Double result = 0d;
            String max = "";
            String min = "";
            BufferedReader bufferedReader = new BufferedReader(new FileReader
                    ("/home/zoheb/java/lunch/src/resources/sales.txt"));
            while ((line = bufferedReader.readLine()) != null) {
                strings = line.split("\n");
                for (String sales : strings
                        ) {
                    count++;
                    aDouble = totalSAlesEachWeek(sales, count);
                    doubles.add(aDouble);
                    max = totalMax(sales);
                    maxList.add(max);
                    min = totalMIn(sales);
                    minList.add(min);
                }
            }
            for (Double totalSales : doubles
                    ) {

                result += totalSales;
            }
            System.out.println(Collections.max(doubles));
            System.out.println(Collections.min(doubles));
            System.out.println("Total sales of all Weeks are :" + result);
            System.out.println("maxSAles " + Collections.max(maxList));
            System.out.println("minSAles " + Collections.min(minList));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //

    }

    private static String totalMIn(String sales) {
        List<String> salesList = Arrays.asList(sales.split(","));
        return Collections.min(salesList);
    }

    private static String totalMax(String sales) {
        List<String> salesList = Arrays.asList(sales.split(","));
        return Collections.max(salesList);

    }

    private static Double totalSAlesEachWeek(String sales,
                                             int count) {
        Double aDouble = 0d;

        List<String> salesList = Arrays.asList(sales.split(","));
        for (String s : salesList
                ) {
            aDouble += Double.parseDouble(s);

        }

        System.out.println("sales of " + count + "st week is " + aDouble);
        System.out.println("Average sales of " + count + "st week is " + aDouble / salesList.size());
        return aDouble;

    }
}
