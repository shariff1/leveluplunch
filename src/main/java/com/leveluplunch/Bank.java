package com.leveluplunch;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Bank {

    private Double accountBalance;
    private Double interestRate;
    private Double lastAmountOfInterestEarned;

    Bank(Double accountBalance,
         Double interestRate,
         Double lastAmountOfInterestEarned) {
        this.accountBalance = accountBalance;
        this.interestRate = interestRate;
        this.lastAmountOfInterestEarned = lastAmountOfInterestEarned;

    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public Double getLastAmountOfInterestEarned() {
        return lastAmountOfInterestEarned;
    }

    void calculateMoney() {
        List<String> deposits = readDepositValues();
        List<String> withdraws = readWithDrawValues();
        for (String deposit : deposits) {
            accountBalance += Double.parseDouble(deposit);
        }
        for (String withdrawMoney : withdraws
                ) {
            accountBalance -= Double.parseDouble(withdrawMoney);
        }

    }

    private List<String> readWithDrawValues() {
        List<String> withdrawList = new ArrayList<String>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader
                    ("/home/zoheb/java/lunch/src/resources/withdrawals.txt"));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                withdrawList.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return withdrawList;
    }

    private List<String> readDepositValues() {
        List<String> depositList = new ArrayList<String>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader
                    ("/home/zoheb/java/lunch/src/resources/deposits.txt"));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                depositList.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return depositList;
    }

    public void calculateInterest() {
        Double monthlyInterest = interestRate / 12;
        lastAmountOfInterestEarned = monthlyInterest * accountBalance;
        accountBalance += lastAmountOfInterestEarned;
    }
}
