package com.leveluplunch;

import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        System.out.println(checkPalindrome());
    }

    private static String checkPalindrome() {
        Scanner scanner = new Scanner(System.in);
        String phrase = scanner.nextLine();
        return isPalindrome(phrase);

    }

    private static String isPalindrome(String phrase) {
        String original = phrase.replaceAll("/s+", "");
        StringBuffer stringBuffer = new StringBuffer(original);
        String reversed = stringBuffer.reverse().toString();
        return reversed.equalsIgnoreCase(original) ? "is palindrome" : "NOT";
    }
}
