package com.leveluplunch;

import java.util.Scanner;

public class ValidateSSN {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        calculateMyIsbn(sc.next());
    }


    private static void calculateMyIsbn(String isbn) {
        int sum = 0;
        if (isbn.length() == 9) {
            for (int i = 0; i <= isbn.length() - 1; i++) {
                sum += i * Character.getNumericValue(isbn.charAt(i));
            }
        }
        int checksum = sum % 11;
        System.out.println(checksum == 10 ? isbn + "X" : isbn + checksum);
    }
}
