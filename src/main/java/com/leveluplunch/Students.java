package com.leveluplunch;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Students {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberofStudent = scanner.nextInt();
        List<Student> studentList = new ArrayList<>();
        for (int i = 0; i < numberofStudent; i++) {
            Student student = new Student();
            student.setName(scanner.next());
            student.setMarks(scanner.nextInt());
            studentList.add(student);
        }
        studentList.stream().sorted((o1, o2) -> Integer.compare(o1.getMarks(), o2.getMarks())).forEach(student ->
                System.out.println("Name" + student.getName() + "--" + "Marks" + student.getMarks()));

    }
}
