package com.leveluplunch;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class HangsMan {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String character = scanner.next();
        List<String> dataBase = new LinkedList<>(Arrays.asList("simplicity", "equality", "grandmother",
                "neighborhood", "relationship", "mathematics",
                "university", "explanation"));
        matchCharacter(character, dataBase, scanner);
    }

    private static void matchCharacter(String character,
                                       List<String> dataBase,
                                       Scanner scanner) {
        String puzzleWord = "";
        List<Character> characterList = new LinkedList<>();
        for (String match : dataBase
                ) {
            puzzleWord = match.contains(character) ? match : null;
            if (puzzleWord != null) {
                break;
            }
        }
        List<Character> firstResult = matchesChar(character, puzzleWord, characterList);
        System.out.println("You have guessed the word right, Lets play");
        System.out.println(" \n  Guess the next word");
        for (int i = 0; i < puzzleWord.length() - 1; i++) {
            System.out.println("\n");
            String nextWord = scanner.next();
            List<Character> list = matchesStars(nextWord, puzzleWord, firstResult);
            for (Character c : list
                    ) {
                System.out.print(c);
                String finalPuzzleWord = puzzleWord;
                dataBase.removeIf(s -> s.equals(finalPuzzleWord));
            }
        }
        System.out.println("Game is finished");
    }

    private static List<Character> matchesStars(String nextWord,
                                                String puzzleWord,
                                                List<Character> firstResult) {
        for (int i = 0; i < puzzleWord.length(); i++) {
            if (nextWord.charAt(0) == puzzleWord.charAt(i)) {
                firstResult.set(i, nextWord.charAt(0));
            }
        }
        return firstResult;
    }

    private static List<Character> matchesChar(String character,
                                               String puzzleWord,
                                               List<Character> characterList
    ) {
        for (int i = 0; i < puzzleWord.length(); i++) {
            if (character.charAt(0) == puzzleWord.charAt(i)) {
                characterList.add(puzzleWord.charAt(i));
            } else {
                characterList.add('*');
            }
        }
        return characterList;
    }

}

