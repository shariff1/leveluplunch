package com.leveluplunch;

import java.util.Scanner;

public class Telephone {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String character = scanner.next();
        String number = generatePhoneNumbers(character);
        System.out.println(number);
    }

    private static String generatePhoneNumbers(String keyboardInput) {
        String normal = "";
        for (Character character : keyboardInput.toUpperCase().toCharArray()) {
            normal += generatenum(character);
        }
        return normal;
    }

    private static char generatenum(Character character) {
        if (Character.isDigit(character)) {
            return character;
        } else {
            switch (character) {
                case 'A':
                case 'B':
                case 'C':
                    return '2';
                case 'D':
                case 'E':
                case 'F':
                    return '3';
                case 'G':
                case 'H':
                case 'I':
                    return '4';
                case 'J':
                case 'K':
                case 'L':
                    return '5';
                case 'M':
                case 'N':
                case 'O':
                    return '6';
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                    return '7';
                case 'T':
                case 'U':
                case 'V':
                    return '8';
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                    return '9';
                default:
                    return '-';
            }
        }
    }

}
