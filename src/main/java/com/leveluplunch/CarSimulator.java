package com.leveluplunch;

public class CarSimulator {
    public static void main(String[] args) {
        CarSimulator carSimulator = new CarSimulator();
        FuelGauge fuelGauge = new FuelGauge();
        Odometer  odometer = new Odometer(0,fuelGauge);
        for (int i = 0; i < FuelGauge.FUEL_GALLON; i++) {
            fuelGauge.addGallons();
        }
        while (fuelGauge.getGallon() > 0){
            odometer.addMilage();
        }
        System.out.println(fuelGauge.getGallon());
        System.out.println(odometer.calMilage());

    }
}
