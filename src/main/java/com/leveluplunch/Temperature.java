package com.leveluplunch;

public class Temperature {
    private Double temperature;

    public Temperature(Double temperature) {
        this.temperature = temperature;
    }

    public Boolean isisEthylFreezing() {
        return temperature < -173.0;
    }

    public Boolean isEthylBoiling() {
        return temperature >= 172.0;
    }

    public Boolean isOxygenFreezing() {
        return temperature < -362.0;
    }

    public Boolean isOxygenBoiling() {
        return temperature >= -306.0;
    }

    public Boolean isWaterFreezing() {
        return temperature < 32.0;
    }

    public Boolean isWaterBoiling() {
        return temperature >= 212.0;
    }
}
