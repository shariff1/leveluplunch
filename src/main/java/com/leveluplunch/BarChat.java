package com.leveluplunch;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BarChat {
    private static final int CHAR_SIZE = 100;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Double sales = scanner.nextDouble();
        List<Double> salesList = new ArrayList<Double>();
        for (int i = 0; i < 5; i++) {
            salesList.add(scanner.nextDouble());
        }
        generateBarChat(salesList);

    }

    private static void generateBarChat(List<Double> salesList) {
        int result = 0;
        for (Double sales : salesList
                ) {
            System.out.println("Store " + getBAr(sales));
        }

    }

    private static String getBAr(Double sales) {
        int numberOfChars = (int) (sales / CHAR_SIZE);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < numberOfChars; i++) {
            stringBuilder.append("*");
        }
        return stringBuilder.toString();
    }
}
