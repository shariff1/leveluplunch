package com.leveluplunch;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DivBy5and6 {
    public static void main(String[] args) {
        generateDiv();
    }

    private static void generateDiv() {
        List<Integer> integerList = new ArrayList<>();
        List<Integer> div5 = new ArrayList<>();
        List<Integer> div6 = new ArrayList<>();

        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            integerList.add(random.nextInt(1000 - 100 + 100) + 100);
        }
        for (Integer number : integerList
                ) {
            if (number % 5 == 0) {
                div5.add(number);
            } else if (number % 6 == 0) {
                div6.add(number);
            }
        }
       /* div5.forEach(System.out::print);
        div6.forEach(System.out::print);*/
        int count = 0;
        for (Integer integer : div5
                ) {
            count = count + 1;
            if (count == 11) {
                System.out.print("\n");
            }
            System.out.print("div 5 " + integer);
        }
        for (Integer integer : div6
                ) {
            System.out.print("div 6 " + integer);
        }

    }
}
