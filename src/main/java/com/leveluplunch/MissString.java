package com.leveluplunch;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MissString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        System.out.println(wordCount(name));
        System.out.println(mostFrequent(name));
        System.out.println(replaceSubstring(scanner));
        System.out.println(arrayToString(name.toCharArray()));
    }

    private static String arrayToString(char[] chars) {
        return String.valueOf(chars);
    }

    private static String replaceSubstring(Scanner scanner) {
        String original = scanner.next();
        String findStrig = scanner.next();
        String replaceString = scanner.next();
        return original.replaceAll("\\\\b" + findStrig + "\\\\b", replaceString);

    }

    private static char mostFrequent(String name) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        char maxCharacter = 0;
        for (int i = 0; i < name.length(); i++) {
            char letter = name.charAt(i);
            map.merge(letter, 1, (a, b) -> a + b);
            int total = Collections.max(map.values());
            for (Map.Entry<Character, Integer> entry : map.entrySet()
                    ) {

                if (entry.getValue() == total) {
                    maxCharacter = entry.getKey();
                }
            }
        }
        return maxCharacter;
    }

    private static int wordCount(String name) {
        int wordCount = 0;
        for (int i = 0; i < name.length(); i++) {
            if (Character.isLetter(name.charAt(i))) {
                wordCount++;
            }
        }
        return wordCount;
    }
}
