package com.leveluplunch;

import java.util.HashSet;
import java.util.Random;

public class EliminateDups {
    public static void main(String[] args) {
        removeDups();
    }
    private static void removeDups() {
        HashSet<Integer> integerList = new HashSet<>();
        Random random = new Random();
        for (int i = 0; i < 89; i++) {
            integerList.add(random.nextInt(100 - 1 + 1) + 1);
        }
        integerList.forEach(System.out::println);
    }
}
