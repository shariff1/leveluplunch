package com.leveluplunch;

import java.util.Scanner;

public class DistanceConverter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int distance = scanner.nextInt();
        DisConverter disConverter = new DisConverter(distance);
        int selection = scanner.nextInt();
        while (selection < 0 || selection > 4) {
            System.out.println("Select between 0 to 4");
            selection = scanner.nextInt();
        }
        switch (selection) {
            case 1:
                System.out.println(disConverter.convertToKms());
                break;
            case 2:
                System.out.println(disConverter.converToinches());
                break;
            case 3:
                System.out.println(disConverter.convertTofeet());
                break;
            case 4:
                System.out.println("Bah-Bye");
                break;
        }
    }
}
