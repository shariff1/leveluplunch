package com.leveluplunch;

import java.util.Scanner;

public class Freezing {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Double temp = scanner.nextDouble();
        Temperature temperature = new Temperature(temp);
        Boolean isEthylFreezing = temperature.isisEthylFreezing();
        Boolean isEthylBoiling = temperature.isEthylBoiling();
        Boolean isOxygenFreezing = temperature.isOxygenFreezing();
        Boolean isOxygenBoiling = temperature.isOxygenBoiling();
        Boolean isWaterFreezing = temperature.isWaterFreezing();
        Boolean isWaterBoiling = temperature.isWaterBoiling();
        System.out.println(isEthylFreezing ? "ethanol is freezing" : "not freezing");
        System.out.println(isEthylBoiling ? "ethanol is boiling" : "not boiling");
        System.out.println(isOxygenFreezing ? "oxygen is freezing" : "not freezing");
        System.out.println(isOxygenBoiling ? "oxygen is boiling" : "not boiling");
        System.out.println(isWaterFreezing ? "water is freezing" : "not freezing");
        System.out.println(isWaterBoiling ? "water is boiling" : "not boiling");

    }
}
