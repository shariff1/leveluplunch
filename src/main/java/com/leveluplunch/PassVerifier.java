package com.leveluplunch;

import java.util.function.Predicate;

public class PassVerifier {
    Predicate<String> hasAtLeastOneDigit = new Predicate<String>() {
        public boolean test(String s) {
            return s.chars().anyMatch(Character::isDigit);
        }
    };
    Predicate<String> hasLengthOfSix = new Predicate<String>() {
        @Override
        public boolean test(String s) {
            return s.length() > 6;
        }
    };
    Predicate<String> hasLowerCase = new Predicate<String>() {
        @Override
        public boolean test(String t) {
            return t.chars().anyMatch(Character::isLowerCase);
        }
    };
    Predicate<String> hasUpperCase = new Predicate<String>() {
        @Override
        public boolean test(String t) {
            return t.chars().anyMatch(Character::isUpperCase);
        }
    };
    private String password;

    public PassVerifier(String password) {
        this.password = password;
    }

    public boolean isValid() {
        return hasUpperCase.and(hasLowerCase).and(hasLengthOfSix).test(this.password);
    }
}
