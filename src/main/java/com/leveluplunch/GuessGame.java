package com.leveluplunch;

import java.util.Random;
import java.util.Scanner;

public class GuessGame {
    public static void main(String[] args) {
        // generate three numbers
        // take user input
        // match sum of generate number to input\
        Scanner scanner = gamePlay();
        System.out.println("would you like to play again, press 0 0r 1");
        switch (scanner.nextInt()) {
            case 0:
                GuessGame.gamePlay();
                break;
            case 1:
                System.out.println("bah bye");
                break;

        }
    }

    private static Scanner gamePlay() {
        int sum = 0;
        for (int i = 0; i < 3; i++) {
            Random random = new Random();
            int randomNumber = random.nextInt(9);
            sum += randomNumber;
            System.out.print(randomNumber + " ");

        }
        System.out.println("what is the answer?");
        Scanner scanner = new Scanner(System.in);
        int answer = scanner.nextInt();
        System.out.println(answer == sum ? "You guessed right" : "wrong");
        return scanner;
    }
}
