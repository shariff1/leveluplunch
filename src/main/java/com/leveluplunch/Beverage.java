package com.leveluplunch;

import java.util.Scanner;

public class Beverage {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfBottles = scanner.nextInt();
        String beverage = scanner.next();
        for (int i = numberOfBottles; i > 0; i--) {
            System.out.println(numberOfBottles + " " + "bottles" + " " + "of" + " " + beverage + " " +
                    "on" + " " + "the" + " " + "wall" + "\n" + numberOfBottles + " " + "bottles" + " " + "of beer" +
                    "." +
                    "\n" + "Take one down and pass it around," + (numberOfBottles - 1) + " " + " bottles of beer on " +
                    "the wall.\n"
            );

            numberOfBottles--;
        }

    }
}
