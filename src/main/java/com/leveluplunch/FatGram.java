package com.leveluplunch;

import java.util.Scanner;

public class FatGram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Double fat = scanner.nextDouble();
        Double caloris = scanner.nextDouble();
        calculatePercentagefromFat(fat, caloris);
    }

    private static void calculatePercentagefromFat(Double fat,
                                                   Double caloris) {
        Double fatCAloris = calculateCaloris(fat);
        Double percentageOfCaloris = fatCAloris / caloris;
        if (percentageOfCaloris < 0.3) {
            System.out.println("foood low in fat");
        }
        if (fatCAloris > percentageOfCaloris) {
            System.out.println("foood NOT low in fat");
        }
    }

    private static Double calculateCaloris(Double fat
    ) {
        return (fat * 9) * 100;

    }
}
