package com.leveluplunch;

public class DisConverter {
    private Integer totolDistance;

    public DisConverter(int distance) {
        this.totolDistance = distance;
    }

    public Double convertToKms() {
        return totolDistance * 0.001;
    }

    public Double converToinches() {
        return totolDistance * 39.37;
    }

    public Double convertTofeet() {
        return totolDistance * 3.287;
    }

}
