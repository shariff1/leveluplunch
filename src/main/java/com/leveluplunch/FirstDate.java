package com.leveluplunch;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class FirstDate {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Prompt user for year
        System.out.print("Enter a year: ");
        int year = input.nextInt();

        input.close();

        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("EEEE, MMM d, y ");

        for (int month = 1; month <= 12; month++) {

            LocalDate date = LocalDate.of(year, Month.of(month), 10);
            LocalDate firstDayOfMonth = date.with(TemporalAdjusters
                    .firstDayOfMonth());

            System.out.println(formatter.format(firstDayOfMonth));
        }
    }
}

