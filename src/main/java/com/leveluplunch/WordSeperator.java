package com.leveluplunch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class WordSeperator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String phrase = scanner.next();
        sepearteTheWord(phrase);
    }

    private static void sepearteTheWord(String phrase) {
        String[] words = phrase.split("(?=\\p{Lu})");
        StringBuilder stringBuilder = new StringBuilder();
        List<String> original = new ArrayList<>();
        original.addAll(Arrays.asList(words));

        List<String> stringList = new ArrayList<>();

        for (int i = 0; i < words.length; i++) {
            if (i == 0) {
                stringList.add(original.get(0));
            } else {
                stringList.add(original.get(i).toLowerCase());
            }
        }
        for (String s : stringList
                ) {
            stringBuilder.append(s + " ");
        }
        System.out.println(stringBuilder);
    }
}
