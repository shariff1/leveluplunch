package com.leveluplunch;

import java.util.Random;

public class Coin {
    private String sideUp;

    public Coin() {
        toss();
    }

    public static void main(String[] args) {
        Coin coin = new Coin();
        int head = 0;
        for (int i = 0; i < 20; i++) {
            coin.toss();
            System.out.println(coin.getSideUp());
            if ("head".equalsIgnoreCase(coin.getSideUp())) {
                head++;
            }
        }
        System.out.println("head " + head);
        System.out.println("tails " + (20 - head));

    }

    private void toss() {
        Random random = new Random();
        int headorTails = random.nextInt(2);
        if (headorTails == 0) {
            this.sideUp = "head";
        } else {
            this.sideUp = "toss";
        }

    }

    public String getSideUp() {
        return sideUp;
    }
}
