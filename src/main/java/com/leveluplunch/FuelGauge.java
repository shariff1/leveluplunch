package com.leveluplunch;

public class FuelGauge {
    static final int FUEL_GALLON = 15;

    private int gallon;

    public FuelGauge() {
        gallon = 0;
    }

    public FuelGauge(int gallon) {
        if (gallon <= FUEL_GALLON) {
            this.gallon = gallon;
        }else {
            gallon = FUEL_GALLON;
        }
    }

    public void addGallons() {
        if (gallon < FUEL_GALLON) {
            gallon++;
        } else if (gallon > FUEL_GALLON) {
            throw new IllegalArgumentException("Galon Overflow");
        }
    }

    public int getGallon() {
        return gallon;
    }
    public void burnFuel(){
        if (gallon>0){
            gallon--;
        }
    }
}
