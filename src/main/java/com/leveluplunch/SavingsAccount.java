package com.leveluplunch;

import java.text.DecimalFormat;
import java.util.Scanner;

public class SavingsAccount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Double interestRate = scanner.nextDouble();
        Bank bank = new Bank(500d, interestRate, 0.0);
        bank.calculateMoney();
        bank.calculateInterest();
        DecimalFormat euro = new DecimalFormat("€0.00");
        System.out.println("currency " + euro.getCurrency().getSymbol());
        System.out.println(euro.format(bank.getAccountBalance()) + " accnt bal");
        System.out.println(euro.format(bank.getInterestRate()) + " int rate");
        System.out.println(euro.format(bank.getLastAmountOfInterestEarned()) + " last in earned");
    }


}
