package com.leveluplunch.pong;

import java.awt.*;

public class HumanPaddle implements Paddle {
    double y, yVel;
    int x, player;
    boolean upAccel, downAccel;
    double GRAVITY = 0.94;

    public HumanPaddle(int player) {
        upAccel = false;
        downAccel = false;
        y = 210;//y is the position
        yVel = 0;
        if (player == 1) {
            x = 20;
        } else {
            x = 660;
        }
    }

    public void setUpAccel(boolean input) {
        this.upAccel = input;
    }

    public void setDownAccel(boolean input) {
        this.downAccel = input;
    }

    @Override
    public void move() {
        if (upAccel) {
            yVel -= 2;
        } else if (downAccel) {
            yVel += 2;
        } else if (!upAccel && !downAccel) {
            yVel *= GRAVITY;
        }
        if (yVel >= 5) yVel = 5;
        else if (yVel <= -5) yVel = -5;

        y += yVel;

        if (y < 0) y = 0;// dont allow paddle to go beyond y (top position)
        // sixe of applet is 500 and paddle is 80 = 500-80- to determine the
        // down part so paddle doesnt go beyond screen
        if (y > 600) y = 600;


    }

    @Override
    public int getY() {
        return (int) y;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, (int) y, 20, 80);// paddle hright and width
    }
}
