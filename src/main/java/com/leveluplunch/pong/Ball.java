package com.leveluplunch.pong;

import java.awt.*;

public class Ball {
    double xVel, yVel, x, y;

    public Ball() {
        x = 350;
        y = 250;
        xVel = -1;
        yVel = 1;
    }

    public void move() {
        x += xVel;
        y += yVel;
        if (y < 10) yVel = -yVel;
        if (y > 490) yVel = -yVel;
    }

    public void checkPaddleCollision(Paddle paddle1,
                                     Paddle paddle2) {
        if (x <= 50) {
            if (y >= paddle1.getY() && y <= paddle1.getY() + 80) {
                xVel = -xVel;
            }

        } else if (x >= 650) {
            if (y >= paddle2.getY() && y <= paddle2.getY() + 80) {
                xVel = -xVel;
            }

        }
    }

    public void draw(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.fillOval((int) x - 10, (int) y - 10, 20, 20);
    }

    public int getX() {
        return (int) x;
    }

    public int getY() {
        return (int) y;
    }
}
