package com.leveluplunch.pong;

import java.awt.*;

public interface Paddle {
    void move();

    int getY();

    void draw(Graphics graphics);

}
