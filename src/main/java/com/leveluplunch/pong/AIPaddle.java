package com.leveluplunch.pong;

import java.awt.*;

public class AIPaddle implements Paddle {
    double y, yVel;
    int x, player;
    boolean upAccel, downAccel;
    double GRAVITY = 0.94;
    Ball b1;

    public AIPaddle(int player,
                    Ball b) {
        b1 = b;
        upAccel = false;
        downAccel = false;
        y = 200;//y is the position
        yVel = 0;
        if (player == 1) {
            x = 30;
        } else {
            x = 600;
        }
    }


    @Override
    public void move() {
        y = b1.getY() - 20;
        if (y < 0) y = 0;
        // dont allow paddle to go beyond y (top position)
        // sixe of applet is 500 and paddle is 80 = 500-80- to determine the
        // down part so paddle doesnt go beyond screen
        if (y > 420) y = 420;
    }

    @Override
    public int getY() {
        return (int) y;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.fillRect(x, (int) y, 40, 80);// paddle hright and width
    }
}
