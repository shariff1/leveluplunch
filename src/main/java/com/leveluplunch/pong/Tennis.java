package com.leveluplunch.pong;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Tennis extends Applet implements Runnable, KeyListener {
    static final int WIDTH = 700, HEIGHT = 900;
    Thread thread;
    HumanPaddle humanPaddle;
    AIPaddle aiPaddle;
    Ball ball;
    boolean gameStarted;

    public void init() {
        gameStarted = false;
        humanPaddle = new HumanPaddle(1);
        ball = new Ball();
        aiPaddle = new AIPaddle(2, ball);
        this.addKeyListener(this);
        this.resize(WIDTH, HEIGHT);
        thread = new Thread(this);
        thread.start();

    }

    public void paint(Graphics graphics) {
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, WIDTH, HEIGHT);
        if (ball.getX() < -10 || ball.getX() > 710) {
            graphics.setColor(Color.red);
            graphics.drawString("Game Over", 350, 250);
        } else {

            humanPaddle.draw(graphics);
            ball.draw(graphics);
            aiPaddle.draw(graphics);
        }
    }

    public void update(Graphics graphics) {
        paint(graphics);
    }

    @Override
    public void run() {
        for (; ; ) {
            if (gameStarted) {
                humanPaddle.move();
                aiPaddle.move();
                ball.move();
            }
            ball.checkPaddleCollision(humanPaddle, aiPaddle);
            repaint();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            humanPaddle.setUpAccel(true);
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            humanPaddle.setDownAccel(true);
        } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            gameStarted = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            humanPaddle.setUpAccel(false);
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            humanPaddle.setDownAccel(false);
        }
    }
}
