package com.leveluplunch;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Vowels {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String phrase = scanner.nextLine();

        //  convert words to char
        // add th char in list
        // for if cher is aeiou => vowels => add this to a list
        //else consonsnts => add this to another list
        // print both the numbers

        convertToChar(phrase);

    }

    private static void convertToChar(String phrase) {
        List<Character> characterList = new ArrayList<>();
        List<Character> vowelList = new ArrayList<>();
        List<Character> consonants = new ArrayList<>();
        for (Character character : phrase.toCharArray()
                ) {
            characterList.add(character);
        }
        for (Character character : characterList
                ) {
            if (character.equals('a') || character.equals('e') || character.equals('i') || character.equals('o') ||
                    character.equals('u')) {
                vowelList.add(character);
            } else {
                consonants.add(character);
            }
        }
        printTheResult(vowelList, consonants);

    }

    private static void printTheResult(List<Character> vowelList,
                                       List<Character> consonants) {
        System.out.println("The Phrase has " + vowelList.size() + " vowels");
        System.out.println("The Phrase has " + consonants.size() + " consonsnats");
    }


}
