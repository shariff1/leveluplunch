package com.leveluplunch;

import java.util.Scanner;

public class Restaurant {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Double totalBill = scanner.nextDouble();
        Double tax = calculateTax(totalBill);
        Double tip = calculateTip(totalBill);
        Double meal = calculateMeal(totalBill, tax, tip);
        System.out.println("total =" + calculateTotal(tax, tip, totalBill));
        System.out.println("tax =" + tax);
        System.out.println("tip =" + tip);
        System.out.println("meal =" + meal);
    }

    private static Double calculateTotal(Double tax,
                                         Double tip,
                                         Double meal) {
        return meal + tip + tax;
    }

    private static Double calculateMeal(Double totalBill,
                                        Double tax,
                                        Double tip) {
        return totalBill - (tax + tip);
    }

    private static Double calculateTip(Double totalBill) {
        return totalBill * 15 / 100;
    }

    private static Double calculateTax(Double totalBill) {
        return totalBill * 6.75 / 100;
    }

}
