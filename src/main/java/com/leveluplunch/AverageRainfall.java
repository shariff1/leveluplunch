package com.leveluplunch;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AverageRainfall {
    public static final int MONTH = 12;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int numberOfYears = scanner.nextInt();
        List<Double> rainFallList = new ArrayList<Double>();
        for (int i = 0; i < numberOfYears; i++) {
            for (int j = 0; j < MONTH; j++) {
                int inchesOfRainFall = scanner.nextInt();
                rainFallList.add(inchesOfRainFall / 1e6d);
            }
        }
        Double avgRain = calculateAverageRain(MONTH, rainFallList);
        Double sumRain = calculateTotalRain(rainFallList);
        System.out.println("avg rain fall for a month " + avgRain + "total inches of rainfall " + sumRain + "display " +
                "no of months " + MONTH);


    }

    private static Double calculateTotalRain(List<Double> rainFallList) {
        Double result = 0d;
        for (Double rain : rainFallList
                ) {
            result += rain;
        }
        return result;

    }

    private static Double calculateAverageRain(int month,
                                               List<Double> inchesOfRainFall) {
        Double result = 0d;
        for (Double rainFall : inchesOfRainFall
                ) {
            result += rainFall / month;
        }
        return result;
    }
}
